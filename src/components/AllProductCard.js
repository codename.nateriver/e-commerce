import { Card, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function ProductCard({productProp}) {
	return(
		<Col lg={3} md={4} sm={6} className="d-inline-flex">
			<Card>
				<Card.Body className="course-card">
					<Card.Title>
						{productProp.name}
					</Card.Title>
					<Card.Text>
						{productProp.description}
					</Card.Text>
					<Card.Text>
						Price: {productProp.price}
					</Card.Text>
				</Card.Body>
				<div>
					<Link to={`update/${productProp._id}`} className="btn btn-dark mb-1">
						Update 
					</Link><br/>
					<Link to={`delete/${productProp._id}`} className="btn btn-dark mb-1">
					Delete
					</Link><br/>
					<Link to={`archive/${productProp._id}`} className="btn btn-dark mb-1">
					Archive
					</Link><br />
					<Link to={`unarchive/${productProp._id}`} className="btn btn-dark mb-1">
					Activate
					</Link><br />
				</div>	
			</Card>	
			
		</Col>
		
		
	);
};