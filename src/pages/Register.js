import { useState, useEffect } from 'react'
import { Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function Register() {


	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ middleInitial, setMiddleInitial ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');

	const [ isActive, setIsActive ] = useState(false);
	const [ isMatched, setIsMatched ] = useState(false);
	const [ isValid, setIsValid ] = useState(false);

	const addressSign = email.search('@');
	const dns = email.search('.com')

	useEffect(() => {
		if (addressSign !== -1 && dns !== -1) {
			setIsValid(true);
			if (password1 === password2 && (password1 !== '' && password2 !== '')) {
				setIsMatched(true);
				if (firstName !== '' && lastName !== '' && middleInitial !== '') {
					setIsActive(true);
				} else {
					setIsActive(false);
				}
			} else {
				setIsMatched(false);
				setIsActive(false);
			}
		} else if (password1 === password2 && (password1 !== '' && password2 !== '')) {
			setIsMatched(true);
		} else {
			setIsActive(false);
			setIsMatched(false);
			setIsValid(false);
		}
	}, [firstName, lastName, middleInitial, email, password1, password2, addressSign, dns, isMatched, isValid, isActive])

	const registerUser = async(eventSubmit) => {
		eventSubmit.preventDefault()

		const isRegistered = await fetch('https://agile-lowlands-37376.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1,
				firstName: firstName,
				lastName: lastName,
				middleInitial: middleInitial
			})
		}).then(res => res.json()).then(data => {
			console.log(data)
			if(data.email) {
				return true;
			} else {
				return false;
			}

		})

		 if (isRegistered) {
		 	
		 		setFirstName('');
		 		setLastName('');
		 		setMiddleInitial('');
		 		setEmail('');
		 		setPassword1('');
		 		setPassword2('');

		 		Swal.fire({
		 			icon: 'success',
		 			title: 'Sign Up Successful',
		 			text: 'Thank you for creating an account',
		 			iconColor: 'black',
		 			confirmButtonColor: 'black',
		 			timer: 5000
		 		}).then(() => {
		 			window.location.href = '/login'
		 		})
		 	
		 } else {
		 	return(
		 		Swal.fire({
		 			icon: 'error',
		 			title: 'Error in creating account',
		 			iconColor: 'black',
		 			confirmButtonColor: 'black'
		 		})
		 	)
		 }
		
	}

	return(
			<div className="sign-up-form">
				<h3 className="">Sign Up</h3>
				<Form onSubmit={e => registerUser(e)}>
					<Form.Group>
						<Form.Label className="form-label"> First Name: </Form.Label>
						<Form.Control className="input-box" type="text" placeholder="Enter your First Name" value={firstName} onChange={event => {setFirstName(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label className="form-label"> Last Name: </Form.Label>
						<Form.Control className="input-box" type="text" placeholder="Enter your Last Name" value={lastName} onChange={event => {setLastName(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label className="form-label"> Middle Initial: </Form.Label>
						<Form.Control className="input-box" type="text" placeholder="Enter your Middle Initial" value={middleInitial} onChange={event => {setMiddleInitial(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label className="form-label"> Email: </Form.Label>
						<Form.Control className="input-box" type="email" placeholder="Enter your Email" value={email} onChange={event => {setEmail(event.target.value)}} required />
						{
							isValid ?
							<h6 className="form-status text-success">Email Valid</h6>
							:
							<>
							<h6 className="form-status text-danger">Invalid Email Format</h6>
							</>
						}
					</Form.Group>

					<Form.Group>
						<Form.Label className="form-label"> Password: </Form.Label>
						<Form.Control className="input-box" type="password" placeholder="Enter your Password" value={password1} onChange={event => {setPassword1(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label className="form-label"> Confirm Password: </Form.Label>
						<Form.Control className="input-box" type="password" placeholder="Confirm your Password" value={password2} onChange={event => {setPassword2(event.target.value)}} required />
						{
							isMatched ?
							<h6 className="form-status text-success">Password Matched</h6>
							:
							<h6 className="form-status text-danger">Password Does Not Matched</h6>
						}
					</Form.Group>

					<Form.Group className="d-flex">
							<Form.Control className="checkbox w-25" type="checkbox" required />
							<Form.Label className="terms-of-services w-75">I agree to the Terms of Services</Form.Label>
					</Form.Group>

					{
						isActive ?
						<Button type="submit" className="button btn-light">
							Sign Up
						</Button>
						:
						<Button className="button btn-light" disabled>
							Sign Up
						</Button>

					}

				</Form>				
			</div>
	);
};
