import { Button, Form} from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function ProductView() {

	const [ productName, setProductName ] = useState('')
	const [ productDescription, setProductDescription ] = useState('')
	const [ productPrice, setProductPrice ] = useState('')

	const {id} = useParams()
	const token = localStorage.getItem('access')

	const UpdateProduct = async(event) => {
		event.preventDefault()

		const isUpdated = await fetch(`https://agile-lowlands-37376.herokuapp.com/products/update/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		}).then(res => res.json()).then(data => {
			if (data) {
				return true
			} else {
				return false
			}
		})
		if (isUpdated) {
			Swal.fire({
				icon: 'success',
				title: 'Product Updated Successfully',
				iconColor: 'black',
				confirmButtonColor: 'black',
			})
			window.location.href = '/admin/products'
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Error in Updating Product',
				iconColor: 'red',
				confirmButtonColor: 'black',
			})
		}
	}

	return(
			<div className="admin-create">
				<Form onSubmit={e => UpdateProduct(e)}>
				<h4 className="text-light">Update Product</h4>
					<Form.Group>
						<Form.Label className=""> Product Name: </Form.Label>
						<Form.Control className="" type="text" placeholder="Product Name" value={productName} onChange={e => {setProductName(e.target.value)}}   required />
					</Form.Group>

					<Form.Group>
						<Form.Label className=""> Description: </Form.Label>
						<Form.Control className="" type="text" placeholder="Product Description" value={productDescription} onChange={e => {setProductDescription(e.target.value)}}  required />
					</Form.Group>

					<Form.Group>
						<Form.Label className=""> Price: </Form.Label>
						<Form.Control className="" type="number" placeholder="Product Price" value={productPrice} onChange={e => {setProductPrice(e.target.value)}}  required />
					</Form.Group>

					<Button type="submit" className="admin-create-btn button btn-light" > Update </Button>
				</Form>
			</div>
	);
};