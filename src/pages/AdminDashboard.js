import {} from 'react-bootstrap'
import {} from 'react-router-dom';

export default function Admin() {

	return(
		<>
		<input type="checkbox" id="check" />
			{/*Header Start*/}
				<header className="header">
				<label htmlFor="check">
					<i className="fa-solid fa-bars" id="sidebar-btn"></i>
				</label>
					<div className="left-area">
						<h3>ADMIN PANEL<span><a href="/logout" className="logout-btn">Logout<i className="fa-solid fa-power-off"></i></a></span></h3>					
					</div>
				</header>

			{/*Sidebar Start*/}
				<div className="sidebar">
					<a href="/admin/products"><i className="admin-logo fa-brands fa-product-hunt"><span>Products</span></i></a>
					<a href="/admin/create"><i className="admin-logo fa-solid fa-plus"><span>Create</span></i></a>
				</div>
		</>	
	);
};