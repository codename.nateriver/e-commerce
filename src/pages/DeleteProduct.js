import { useParams, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function DeleteProduct() {
	const {id} = useParams()
	const token = localStorage.getItem('access')

	const isDeleted = fetch(`https://agile-lowlands-37376.herokuapp.com/products/${id}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
		if (data) {
			return true
		} else {
			return false
		}
	})
	if (isDeleted) {
		Swal.fire({
			icon: 'success',
			title: 'Product Deleted Successfully',
			iconColor: 'black',
			confirmButtonColor: 'black',
		})
		
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Error in Deleting Product',
			iconColor: 'red',
			confirmButtonColor: 'black',
		})
	}
	return(
		<Navigate to='/admin/products' />
	)
};