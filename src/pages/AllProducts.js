import { useState, useEffect } from 'react';
import AllProductCard from '../components/AllProductCard'
import { Link } from 'react-router-dom';

export default function AllProducts() {
	const [ productCollection, setProductCollection ] = useState([]);

	useEffect(() =>{
		let token = localStorage.getItem('access')
		fetch('https://agile-lowlands-37376.herokuapp.com/products/all', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}	
		}).then(res => res.json()).then(data => {
			setProductCollection(data.map(product => {
				return(
					<AllProductCard key={product._id} productProp={product} />
				)
			}))
		})	
	}, [productCollection])
			

	return(
		
		<>
		<div className="create-btn">
		<Link to='/admin/create' className="button btn-dark"> Create</Link>
		</div>
		<div className="product-page">
			{productCollection}
		</div>
		</>
		
	)
}