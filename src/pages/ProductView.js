import { Row, Col, Container, Card, Button} from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';

import UserContext from '../UserContext'

export default function ProductView() {
	const { user } = useContext(UserContext)

	const [ productInfo, setProductInfo ] = useState({
		name: null,
		description: null,
		price: null
	})

	const {id} = useParams()

	useEffect(() => {
		fetch(`https://agile-lowlands-37376.herokuapp.com/products/${id}`).then(res => res.json()).then(data => {

			setProductInfo({
				name: data.name,
				description: data.description,
				price: data.price
			})
		})
	})

	const buy = () => {
		return(
			Swal.fire(
			{
				icon: 'success',
				title: 'Thank you for Buying this Product',
				text: ''
			})

		);
	};

	return(
		<>
			<div className="welcome">
				<Row>
					<Col>
						<Container>
							<Card className="text-center">
								<Card.Body>
									<Card.Title>
										<h2> {productInfo.name} </h2>
									</Card.Title>
									<Card.Subtitle>
										<h6 className="mt-4"> Description: </h6>
									</Card.Subtitle>
									<Card.Text>
										{productInfo.description}
									</Card.Text>
									<Card.Subtitle>
										<h6 className="mt-4"> PHP: </h6>
									</Card.Subtitle>
									<Card.Text>
										{productInfo.price}
									</Card.Text>
								</Card.Body>
							</Card>

							{
								user.id !== null ?
								<Button variant="dark" className="btn-block" onClick={buy}>
									Buy
								</Button>
								:
								<Link className="btn btn-dark btn-block mb-5" to="/login">
									Login to Buy
								</Link>
							}

						</Container>
					</Col>
				</Row>
			</div>
		</>
	);
};