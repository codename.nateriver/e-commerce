import { useParams, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ArchiveProduct() {
	const {id} = useParams()
	const token = localStorage.getItem('access')

	const isArchived = fetch(`https://agile-lowlands-37376.herokuapp.com/products/archive/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
		if (data) {
			return true
		} else {
			return false
		}
	})
	if (isArchived) {
		Swal.fire({
			icon: 'success',
			title: 'Product Archived Successfully',
			iconColor: 'black',
			confirmButtonColor: 'black',
		})
		
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Error in Archiving Product',
			iconColor: 'red',
			confirmButtonColor: 'black',
		})
	}
	return(
		<Navigate to='/admin/products' />
	)
};