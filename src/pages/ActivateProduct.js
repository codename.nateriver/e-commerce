import { useParams, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UnarchiveProduct() {
	const {id} = useParams()
	const token = localStorage.getItem('access')

	const isUnarchived = fetch(`https://agile-lowlands-37376.herokuapp.com/products/unarchive/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
		if (data) {
			return true
		} else {
			return false
		}
	})
	if (isUnarchived) {
		Swal.fire({
			icon: 'success',
			title: 'Product Activated Successfully',
			iconColor: 'black',
			confirmButtonColor: 'black',
		})
		
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Error in Activating Product',
			iconColor: 'red',
			confirmButtonColor: 'black',
		})
	}
	return(
		<Navigate to='/admin/products' />
	)
};